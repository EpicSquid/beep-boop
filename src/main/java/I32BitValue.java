public interface I32BitValue {

  boolean[] getBitArray();

  void createFromBitArray(boolean[] bitArray);
}
