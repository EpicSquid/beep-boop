import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ArithmeticUtil {

  // Stores the different instances of Arithmetic Util
  private static Map<Integer, ArithmeticUtil> instances = new HashMap<>();

  /**
   * Gets an instance of Arithmetic Util for the current bit length. If one does not already exist, it creates one.
   *
   * This reduces the overhead caused by hundreds of objects and is more memory/computationally efficient.
   * @param bitLength The number of bits the perform arithmetic with
   * @return An instance of Arithmetic Util for the bit length specified
   */
  public static ArithmeticUtil getInstance(int bitLength) {
    if (instances.containsKey(bitLength)) {
      return instances.get(bitLength);
    }
    ArithmeticUtil au = new ArithmeticUtil(bitLength);
    instances.put(bitLength, au);
    return au;
  }

  // Stores the value of 1 for this bit length for conversion to 2'C
  private final boolean[] one;
  private final int bitLength;

  /**
   * Creates an Arithmetic Util object with the specified bit length
   * @param bitLength The number of bits to perform arithmetic with
   */
  private ArithmeticUtil(int bitLength) {
    this.bitLength = bitLength;
    one = new boolean[bitLength];
    one[bitLength - 1] = true;
  }

  /**
   * Converts the given bit array to it's complement (i.e. flipts all the bits
   * @param bitArray The array to flip
   * @return The bit flipped array
   */
  public boolean[] convertToOnesComplement(boolean[] bitArray) {
    boolean[] result = new boolean[bitArray.length];
    for (int i = 0; i < bitArray.length; i++) {
      result[i] = !bitArray[i];
    }
    return result;
  }

  /**
   * Negates the bit array using 2'C
   * @param bitArray The array to negate
   * @return The negated array
   */
  public boolean[] negate(boolean[] bitArray) {
    boolean[] res = convertToOnesComplement(bitArray);
    return add(res, one);
  }

  /**
   * Adds 2 bit arrays of the given length.
   * @param a Array 1
   * @param b Array 2
   * @param bitLength The bit length of the array to output
   * @return The 2 arrays added togeher
   */
  private boolean[] add(boolean[] a, boolean[] b, int bitLength) {
    boolean[] result = new boolean[bitLength];
    boolean[] carryOut = new boolean[bitLength + 1];
    boolean[] carryIn = new boolean[bitLength + 1];

    for (int i = bitLength - 1; i >= 0; i--) {
      carryIn[i] = carryOut[i + 1];
      boolean or = a[i] || b[i];
      boolean xor = a[i] ^ b[i];

      boolean xorCarry = xor ^ carryIn[i];
      result[i] = xorCarry;

      if (or != xor || (xor != xorCarry && xor)) {
        carryOut[i] = true;
      }
    }

    System.out.println("Addition was performed! Carry Is: " + arrayToString(carryIn));
    if (carryIn[carryIn.length - 1] != carryOut[carryOut.length - 1]) {
      System.out.println("Overflow has occurred!");
      ControlUnit.getInstance().setOverflow(true);
    } else {
      ControlUnit.getInstance().setOverflow(false);
    }

    return result;
  }

  /**
   * Converts a bit array to a bit String
   */
  public String arrayToString(boolean[] array) {
    StringBuilder sb = new StringBuilder();

    for (boolean bit : array) {
      sb.append(bit ? 1 : 0);
    }

    return sb.toString();
  }

  /**
   * Adds two bit arrays using the default bit length
   * @param a Array 1
   * @param b Array 2
   * @return The sum of a and b in binary
   */
  public boolean[] add(boolean[] a, boolean[] b) {
    return add(a, b, bitLength);
  }

  /**
   * Subtracts bit array b from a in 2'C
   * @param a The array to subtract from
   * @param b The amount to subtract
   * @return The bit array of the subtraction of b from a
   */
  public boolean[] subtract(boolean[] a, boolean[] b) {
    return add(a, negate(b));
  }

  /**
   * Subtracts bit array b from a
   * @param a The array to subtract from
   * @param b The amount to subtract
   * @return The bit array of the subtraction of b from a
   */
  public boolean[] minus(boolean[] a, boolean[] b) {
    boolean[] result = new boolean[bitLength];

    for (int i = bitLength - 1; i >= 0; i--) {
      if (a[i] && !b[i]) {
        result[i] = true;
      } else if (!a[i] && !b[i]) {
        result[i] = false;
      } else if (a[i] && b[i]) {
        result[i] = false;
      } else if (!a[i] && b[i]) {
        borrow(a, i);

        result[i] = true;
      }
    }

    return result;
  }

  /**
   * "Borrows" the next value along the bit array
   * @param a The binary number to find the next number to borrow from
   * @param i The current index needing to borrow
   */
  private void borrow(boolean[] a, int i) {
    if (i > 0 && a[i - 1]) {
      a[i - 1] = false;
      a[i] = true;
    } else if (i > 0) {
      borrow(a, i - 1);
      a[i - 1] = true;
      a[i] = true;
    }
  }

  /**
   * Multiplies two binary arrays
   * @param m Multiplicand
   * @param r Multiplier
   * @return The binary array of the multiplied values
   */
  public boolean[] multiply(boolean[] m, boolean[] r) {
    boolean[] a = new boolean[bitLength * 2 + 1];
    boolean[] s = new boolean[bitLength * 2 + 1];
    boolean[] p = new boolean[bitLength * 2 + 1];

    boolean[] negativeM = negate(m);

    for (int i = 0; i < bitLength; i++) {
      a[i] = m[i];
      s[i] = negativeM[i];
      p[i + bitLength] = r[i];
    }

    for (int i = 0; i < bitLength; i++) {
      boolean leastSignificantP = p[p.length - 1];
      boolean secondLeastSignificantP = p[p.length - 2];

      boolean[] val;
      if (leastSignificantP && !secondLeastSignificantP) { // 01
        val = add(p, a, p.length);
      } else if (!leastSignificantP && secondLeastSignificantP) { // 10
        val = add(p, s, p.length);
      } else { // 00 or 11
        val = p;
      }
      p = rShift(val);
    }

    return Arrays.copyOf(p, p.length - 1);
  }

  /**
   * Performs integer division in 2'C
   * @param n Dividend
   * @param d Divisor
   * @return The dividend
   */
  public boolean[] divideInt(boolean[] n, boolean[] d) {

    boolean negative = false;

    if (n[0]) {
      n = negate(n);
      negative = !negative;
    }
    if (d[0]) {
      d = negate(d);
      negative = !negative;
    }

    boolean[] q = new boolean[bitLength];
    boolean[] r = new boolean[bitLength];

    for (int i = bitLength - 1; i >= 0; i--) {
      r = lShift(r);
      r[bitLength - 1] = n[bitLength - i - 1];

      if (isGreaterOrEqual(r, d)) {
        r = subtract(r, d);
        q[bitLength - i - 1] = true;
      }
    }

    System.out.println("Division was performed! Remainder is: " + arrayToString(r));

    return negative ? negate(q) : q;
  }

  /**
   * Performs integer division
   * @param n Dividend
   * @param d Divisor
   * @return The dividend
   */
  public boolean[] divideFloat(boolean[] n, boolean[] d) {
    boolean[] q = new boolean[bitLength];
    boolean[] r = new boolean[bitLength];

    for (int i = bitLength - 1; i >= 0; i--) {
      r = lShift(r);
      r[bitLength - 1] = n[bitLength - i - 1];

      if (isGreaterOrEqual(r, d)) {
        r = subtract(r, d);
        q[bitLength - i - 1] = true;
      }
    }

    System.out.println("Division was performed! Remainder is: " + arrayToString(r));

    return q;
  }

  /**
   * Checks if a given binary array is greater than or equal to the other
   * @return a >= b
   */
  public boolean isGreaterOrEqual(boolean[] a, boolean[] b) {
    for (int i = 0; i < bitLength; i++) {
      if (a[i] && !b[i]) {
        return true;
      } else if (!a[i] && b[i]) {
        return false;
      }
    }
    return true;
  }

  /**
   * Performs a right shift by 1
   */
  public boolean[] rShift(boolean[] a) {
    boolean[] result = new boolean[a.length];
    for (int i = 0; i < a.length; i++) {
      if (i + 1 < a.length) {
        result[i + 1] = a[i];
      }
    }
    return result;
  }

  /**
   * Performs a left shift by 1
   */
  public boolean[] lShift(boolean[] a) {
    boolean[] result = new boolean[a.length];
    for (int i = 0; i < a.length; i++) {
      if (i - 1 >= 0) {
        result[i - 1] = a[i];
      }
    }
    return result;
  }

  public static void setZero(boolean[] result) {
    for (boolean b : result) {
      if (b) {
        ControlUnit.getInstance().setZero(false);
        return;
      }
    }
    ControlUnit.getInstance().setZero(true);
  }

  public static void setFloatZero(boolean[] result) {
    result[0] = false;
    setZero(result);
  }
}
