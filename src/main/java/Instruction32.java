public class Instruction32 implements I32BitValue {

  private static final int BIT_LENGTH = 8;

  private boolean[] opCode;
  private boolean[] value1;
  private boolean[] value2;
  private boolean[] value3;

  public Instruction32() {
    opCode = new boolean[BIT_LENGTH];
    value1 = new boolean[BIT_LENGTH];
    value2 = new boolean[BIT_LENGTH];
    value3 = new boolean[BIT_LENGTH];
  }

  public boolean[] getOpCode() {
    return opCode;
  }

  public void setOpCode(boolean[] opCode) {
    this.opCode = opCode;
  }

  public boolean[] getValue1() {
    return value1;
  }

  public void setValue1(boolean[] value1) {
    this.value1 = value1;
  }

  public boolean[] getValue2() {
    return value2;
  }

  public void setValue2(boolean[] value2) {
    this.value2 = value2;
  }

  public boolean[] getValue3() {
    return value3;
  }

  public void setValue3(boolean[] value3) {
    this.value3 = value3;
  }

  @Override
  public boolean[] getBitArray() {
    boolean[] bitArray = new boolean[BIT_LENGTH * 4];

    for (int i = 0; i < BIT_LENGTH; i++) {
      bitArray[i] = opCode[i];
    }

    for (int i = 0; i < BIT_LENGTH; i++) {
      bitArray[i + BIT_LENGTH] = value1[i];
    }

    for (int i = 0; i < BIT_LENGTH; i++) {
      bitArray[i + BIT_LENGTH + BIT_LENGTH] = value2[i];
    }

    for (int i = 0; i < BIT_LENGTH; i++) {
      bitArray[i + BIT_LENGTH + BIT_LENGTH + BIT_LENGTH] = value3[i];
    }

    return bitArray;
  }

  @Override
  public void createFromBitArray(boolean[] bitArray) {
    for (int i = 0; i < BIT_LENGTH; i++) {
      opCode[i] = bitArray[i];
    }

    for (int i = 0; i < BIT_LENGTH; i++) {
      value1[i] = bitArray[i + BIT_LENGTH];
    }

    for (int i = 0; i < BIT_LENGTH; i++) {
      value2[i] = bitArray[i + BIT_LENGTH + BIT_LENGTH];
    }

    for (int i = 0; i < BIT_LENGTH; i++) {
      value3[i] = bitArray[i + BIT_LENGTH + BIT_LENGTH + BIT_LENGTH];
    }
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    for (boolean bit : getBitArray()) {
      sb.append(bit ? 1 : 0);
    }

    return sb.toString();
  }
}
