import java.util.ArrayList;
import java.util.List;

public class ControlUnit {

  private boolean[] counter; // PC
  private boolean[] status; // PSW
  private boolean[] instruction; // IR

  private static final int ZERO = 0;
  private static final int OVERFLOW = 1;

  private static ControlUnit instance;

  private List<ITickListener> listeners;

  public static ControlUnit getInstance() {
    if (instance == null) {
      instance = new ControlUnit();
    }
    return instance;
  }

  private ControlUnit() {
    Int32 count = Int32.parseInt32(20);
    counter = count.getBitArray();
    status = new boolean[Int32.BIT_LENGTH];
    instruction = new boolean[Int32.BIT_LENGTH];
    listeners = new ArrayList<>();
  }

  public void setOverflow(boolean overflow) {
    status[OVERFLOW] = overflow;
  }

  public void setZero(boolean zero) {
    status[ZERO] = zero;
  }

  public void tick() {
    Int32 count = new Int32();
    count.createFromBitArray(counter);

    for (ITickListener listener : listeners) {
      listener.onTick();
    }

    count = new Int32();
    count.createFromBitArray(counter);
    count = Int32.add(count, Int32.parseInt32(1), true);
    counter = count.getBitArray();
  }

  public void addListener(ITickListener listener) {
    listeners.add(listener);
  }

  public void setCount(int count) {
    Int32 newCount = Int32.parseInt32(count);
    counter = newCount.getBitArray();
  }

  public boolean isZero() {
    return status[ZERO];
  }

  public boolean isOverflow() {
    return status[OVERFLOW];
  }

  public boolean[] getCount() {
    return counter;
  }

  public void setInstruction(boolean[] instruction) {
    this.instruction = instruction;
  }

  public boolean[] getInstruction() {
    return instruction;
  }

  public boolean[] getStatus() {
    return status;
  }

  public String toString(boolean[] bitArray) {
    StringBuilder sb = new StringBuilder();

    for (boolean bit : bitArray) {
      sb.append(bit ? 1 : 0);
    }

    return sb.toString();
  }
}
