import java.util.Arrays;

public class Float32 implements I32BitValue {

  /**
   * Sets the type of data being entered for parsing by Float32
   */
  public enum ValueType {

    DECIMAL,
    BINARY,

    ;

  }

  private static final int EXPONENT_LENGTH = 8;
  private static final int SIGNIFICAND_LENGTH = 23;

  private boolean sign;
  private final boolean[] exponent;
  private final boolean[] significand;

  private static final ArithmeticUtil sigArithmeticUtil = ArithmeticUtil.getInstance(SIGNIFICAND_LENGTH);

  public boolean getSign() {
    return sign;
  }

  public boolean[] getExponent() {
    return exponent;
  }

  public boolean[] getSignificand() {
    return significand;
  }

  public Float32() {
    this.sign = false;
    this.exponent = new boolean[EXPONENT_LENGTH];
    this.significand = new boolean[SIGNIFICAND_LENGTH];
  }

  private Float32(boolean sign, boolean[] exponent, boolean[] significand) {
    this.sign = sign;
    this.exponent = exponent;
    this.significand = significand;
  }

  /**
   * Converts a String value into a Float32
   * @param num The string value of the number
   * @param type The type. DECIMAL for decimal numbers, BINARY for a correct SP Float in binary
   * @return
   */
  public static Float32 parseFloat32(String num, ValueType type) {
    if (type == ValueType.DECIMAL) {

      String[] integers = num.split("\\.");
      if (integers.length == 2) {

        boolean sign = false;

        if (integers[0].charAt(0) == '-') {
          sign = true;
          char[] stringToChar = integers[0].toCharArray();
          char[] stringWithoutNegative = new char[stringToChar.length - 1];
          for (int i = 0; i < stringWithoutNegative.length; i++) {
            stringWithoutNegative[i] = stringToChar[i + 1];
          }
          integers[0] = String.copyValueOf(stringWithoutNegative);
        }

        boolean[] left = intToBinary(Integer.parseInt(integers[0]));
        boolean[] right = decimalToBinary(integers[1]);

        int firstOne = -1;
        boolean[] tempSignificand = new boolean[SIGNIFICAND_LENGTH * 2];
        for (int i = 0; i < tempSignificand.length; i++) {
          if (i < SIGNIFICAND_LENGTH) {
            tempSignificand[i] = left[i];
          } else {
            tempSignificand[i] = right[i - SIGNIFICAND_LENGTH];
          }

          if (tempSignificand[i] && firstOne == -1) {
            firstOne = i;
          }
        }

        for (int i = 0; i < tempSignificand.length; i++) {
          if (!tempSignificand[0]) {
            tempSignificand = sigArithmeticUtil.lShift(tempSignificand);
          } else {
            break;
          }
        }

        tempSignificand = sigArithmeticUtil.lShift(tempSignificand);

        int exp = 127 + left.length - firstOne - 1;
        return new Float32(sign, intToExponentBinary(exp), Arrays.copyOf(tempSignificand, SIGNIFICAND_LENGTH));
      }
      return null;
    } else if (type == ValueType.BINARY) {
      char[] chars = num.toCharArray();
      boolean sign = false;
      boolean[] exp = new boolean[EXPONENT_LENGTH];
      boolean[] sig = new boolean[SIGNIFICAND_LENGTH];

      for (int i = 0; i < chars.length; i++) {
        boolean val =  chars[i] == '1';
        if (i == 0) {
          sign = val;
        } else if (i < EXPONENT_LENGTH + 1) {
          exp[i - 1] = val;
        } else {
          sig[i - EXPONENT_LENGTH - 1] = val;
        }
      }

      return new Float32(sign, exp, sig);
    }
    return null;
  }

  private static boolean[] intToBinary(int val) {
    boolean[] result = new boolean[SIGNIFICAND_LENGTH];

    int num = val;

    for (int index = result.length - 1; index >= 0; index--) {
      result[index] = num % 2 != 0;
      num = num / 2;
    }

    return result;
  }

  private static boolean[] decimalToBinary(String val) {
    boolean[] result = new boolean[SIGNIFICAND_LENGTH];

    int offset = 0;
    char[] chars = val.toCharArray();

    for (int i = 0; i < chars.length; i++) {
      if (chars[i] != '0') {
        offset = i;
        break;
      }
    }

    int cap = 10;

    int num = Integer.parseInt(val);

    while (cap <= num) {
      cap *= 10;
    }

    cap = offset > 0 ? cap * (int) Math.pow(10, offset) : cap;

    for (int i = 0; i < result.length; i++) {
      num *= 2;
      if (num >= cap) {
        result[i] = true;
        num -= cap;
      }
    }

    return result;
  }

  private static boolean[] intToExponentBinary(int val) {
    boolean[] result = new boolean[EXPONENT_LENGTH];

    int num = val;

    for (int index = result.length - 1; index >= 0; index--) {
      result[index] = num % 2 != 0;
      num = num / 2;
    }

    return result;
  }

  public static Float32 add(Float32 a, Float32 b) {
    System.out.println("Performing SP Float Addition of " + a.toString() + " and " + b.toString());

    ArithmeticUtil expArithmeticUtil = ArithmeticUtil.getInstance(a.getExponent().length);

    boolean isNegative = false;
    if (a.getSign() && b.getSign()) {
      isNegative = true;
      a.sign = false;
      b.sign = false;
    } else if (a.getSign() && !b.getSign()) {
      a.sign = false;
      return subtract(b, a);
    } else if (!a.getSign() && b.getSign()) {
      b.sign = false;
      return subtract(a, b);
    }

    if (Arrays.equals(a.getExponent(), b.getExponent())) {
      Float32 result = new Float32(isNegative, a.getExponent(), sigArithmeticUtil.add(a.getSignificand(), b.getSignificand()));
      ArithmeticUtil.setFloatZero(result.getBitArray());
      return result;
    } else if (expArithmeticUtil.isGreaterOrEqual(a.getExponent(), b.getExponent())) {
      return addFloats(a, b, isNegative);
    } else {
      return addFloats(b, a, isNegative);
    }
  }

  public static Float32 subtract(Float32 a, Float32 b) {
    System.out.println("Performing SP Float Subtraction of " + a.toString() + " and " + b.toString());

    ArithmeticUtil expArithmeticUtil = ArithmeticUtil.getInstance(a.getExponent().length);

    if (a.getSign() && b.getSign()) {
      a.sign = false;
      b.sign = false;
      return subtract(b, a);
    } else if (a.getSign() && !b.getSign()) {
      b.sign = true;
      return add(a, b);
    } else if (!a.getSign() && b.getSign()) {
      b.sign = false;
      return add(a, b);
    }

    if (expArithmeticUtil.isGreaterOrEqual(a.getExponent(), b.getExponent())) {
      return subFloats(a, b, false);
    } else {
      return subFloats(b, a, true);
    }
  }

  private static Float32 addFloats(Float32 a, Float32 b, boolean isNegative) {
    ArithmeticUtil expArithmeticUtil = ArithmeticUtil.getInstance(a.getExponent().length);
    boolean[] one = { false, false, false, false, false, false, false, true };
    int distance = 0;
    boolean[] tempExp = b.getExponent();

    while (!Arrays.equals(a.getExponent(), tempExp)) {
      tempExp = expArithmeticUtil.add(tempExp, one);
      distance++;
    }
    if (distance > SIGNIFICAND_LENGTH) {
      return a;
    }

    boolean[] expDiff = intToExponentBinary((int) Math.pow(2, distance));
    boolean[] expDiffSignificandLength = new boolean[SIGNIFICAND_LENGTH + 1];

    for (int i = 0; i < expDiff.length; i++) {
      expDiffSignificandLength[SIGNIFICAND_LENGTH - i] = expDiff[expDiff.length - i - 1];
    }

    ArithmeticUtil au2 = ArithmeticUtil.getInstance(SIGNIFICAND_LENGTH + 1);

    boolean[] a1 = new boolean[SIGNIFICAND_LENGTH + 1];
    boolean[] b1 = new boolean[SIGNIFICAND_LENGTH + 1];

    for (int i = 0; i < SIGNIFICAND_LENGTH; i++) {
      a1[i + 1] = a.getSignificand()[i];
      b1[i + 1] = b.getSignificand()[i];
    }

    a1[0] = true;
    b1[0] = true;

    boolean[] sigResult = au2.divideFloat(b1, expDiffSignificandLength);
    sigResult = Arrays.copyOfRange(sigResult, 1, sigResult.length);

    Float32 result = new Float32(isNegative, a.getExponent(), sigArithmeticUtil.add(a.getSignificand(), sigResult));
    ArithmeticUtil.setFloatZero(result.getBitArray());
    return result;
  }

  private static Float32 subFloats(Float32 a, Float32 b, boolean isNegative) {
    ArithmeticUtil expArithmeticUtil = ArithmeticUtil.getInstance(a.getExponent().length);
    boolean[] one = { false, false, false, false, false, false, false, true };
    int distance = 0;
    boolean[] tempExp = b.getExponent();

    while (!Arrays.equals(a.getExponent(), tempExp)) {
      tempExp = expArithmeticUtil.add(tempExp, one);
      distance++;
    }
    if (distance > SIGNIFICAND_LENGTH) {
      return a;
    }

    boolean[] expDiff = intToExponentBinary((int) Math.pow(2, distance));
    boolean[] expDiffSignificandLength = new boolean[SIGNIFICAND_LENGTH + 1];

    for (int i = 0; i < expDiff.length; i++) {
      expDiffSignificandLength[SIGNIFICAND_LENGTH - i] = expDiff[expDiff.length - i - 1];
    }

    ArithmeticUtil au2 = ArithmeticUtil.getInstance(SIGNIFICAND_LENGTH + 1);

    boolean[] a1 = new boolean[SIGNIFICAND_LENGTH + 1];
    boolean[] b1 = new boolean[SIGNIFICAND_LENGTH + 1];

    for (int i = 0; i < SIGNIFICAND_LENGTH; i++) {
      a1[i + 1] = a.getSignificand()[i];
      b1[i + 1] = b.getSignificand()[i];
    }

    a1[0] = true;
    b1[0] = true;

    boolean[] sigResult = au2.divideFloat(b1, expDiffSignificandLength);
    boolean[] subtracted = au2.minus(a1, sigResult);

    int newFirstValue = subtracted.length;

    for (int i = 0; i < subtracted.length; i++) {
      if (subtracted[i]) {
        newFirstValue = i;
        break;
      }
    }

    boolean[] exponent = Arrays.copyOf(a.getExponent(), a.getExponent().length);

    if (newFirstValue > 0 && newFirstValue < subtracted.length) {
      exponent = expArithmeticUtil.minus(exponent, intToExponentBinary(newFirstValue));
      for (int i = -1; i < newFirstValue; i++) {
        subtracted = au2.lShift(subtracted);
      }
    }

    subtracted = Arrays.copyOf(subtracted, SIGNIFICAND_LENGTH);

    Float32 result = new Float32(isNegative, exponent, subtracted);
    ArithmeticUtil.setFloatZero(result.getBitArray());
    return result;
  }

  public static Float32 multiply(Float32 a, Float32 b) {
    System.out.println("Performing SP Float Multiplication of " + a.toString() + " and " + b.toString());

    ArithmeticUtil expArithmeticUtil = ArithmeticUtil.getInstance(a.getExponent().length);
    ArithmeticUtil au2 = ArithmeticUtil.getInstance(SIGNIFICAND_LENGTH + 1);

    boolean[] a1 = new boolean[SIGNIFICAND_LENGTH + 1];
    boolean[] b1 = new boolean[SIGNIFICAND_LENGTH + 1];

    for (int i = 0; i < SIGNIFICAND_LENGTH; i++) {
      a1[i + 1] = a.getSignificand()[i];
      b1[i + 1] = b.getSignificand()[i];
    }

    a1[0] = true;
    b1[0] = true;

    boolean[] multiply = au2.multiply(a1, b1);
    boolean[] result = new boolean[SIGNIFICAND_LENGTH];

    boolean[] expBias = { false, true, true, true, true, true, true, true };
    boolean[] exp = expArithmeticUtil.add(a.getExponent(), b.getExponent());
    exp = expArithmeticUtil.minus(exp, expBias);
    int firstValue = -1;

    for (int i = 0; i < multiply.length; i++) {
      if (multiply[i] && firstValue == -1) {
        firstValue = i;
        continue;
      }
      if (firstValue > -1 && i - firstValue - 1 < SIGNIFICAND_LENGTH) {
        result[i - firstValue - 1] = multiply[i];
      }
    }

    if (firstValue >= SIGNIFICAND_LENGTH) {
      boolean[] expDiff = intToExponentBinary(SIGNIFICAND_LENGTH - firstValue);
      exp = expArithmeticUtil.add(exp, expDiff);
    }

    Float32 floatResult = new Float32(a.getSign() ^ b.getSign(), exp, result);
    ArithmeticUtil.setFloatZero(floatResult.getBitArray());
    return floatResult;
  }

  public static Float32 divide(Float32 n, Float32 d) {
    System.out.println("Performing SP Float Division of " + n.toString() + " and " + d.toString());

    ArithmeticUtil expArithmeticUtil = ArithmeticUtil.getInstance(n.getExponent().length);
    ArithmeticUtil au2 = ArithmeticUtil.getInstance(SIGNIFICAND_LENGTH + 1);

    boolean[] n1 = new boolean[SIGNIFICAND_LENGTH + 1];
    boolean[] d1 = new boolean[SIGNIFICAND_LENGTH + 1];

    for (int i = 0; i < SIGNIFICAND_LENGTH; i++) {
      n1[i + 1] = n.getSignificand()[i];
      d1[i + 1] = d.getSignificand()[i];
    }

    n1[0] = true;
    d1[0] = true;

    boolean[] result = au2.divideFloat(n1, d1);
    boolean[] expBias = { false, true, true, true, true, true, true, true };
    boolean[] exp = expArithmeticUtil.minus(n.getExponent(), d.getExponent());
    exp = expArithmeticUtil.add(exp, expBias);

    Float32 floatResult = new Float32(n.getSign() ^ d.getSign(), exp, Arrays.copyOf(result, 23));
    ArithmeticUtil.setFloatZero(floatResult.getBitArray());
    return floatResult;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append(sign ? '1' : '0');

    for (boolean b : exponent) {
      sb.append(b ? '1' : '0');
    }

    for (boolean b : significand) {
      sb.append(b ? '1' : '0');
    }

    return sb.toString();
  }

  @Override
  public boolean[] getBitArray() {
    boolean[] bitArray = new boolean[EXPONENT_LENGTH + SIGNIFICAND_LENGTH + 1];

    bitArray[0] = sign;

    for (int i = 0; i < EXPONENT_LENGTH; i++) {
      bitArray[i + 1] = exponent[i];
    }
    for (int i = 0; i < SIGNIFICAND_LENGTH; i++) {
      bitArray[i + 1 + EXPONENT_LENGTH] = significand[i];
    }

    return bitArray;
  }

  @Override
  public void createFromBitArray(boolean[] bitArray) {
    sign = bitArray[0];

    for (int i = 0; i < EXPONENT_LENGTH; i++) {
      exponent[i] = bitArray[i + 1];
    }
    for (int i = 0; i < SIGNIFICAND_LENGTH; i++) {
      significand[i] = bitArray[i + 1 + EXPONENT_LENGTH];
    }
  }
}
