import java.util.Arrays;

public enum InstructionSet {

  NONE() {
    @Override
    public void execute(Instruction32 instruct) {

    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) {
      return null;
    }

    @Override
    public String instructionToString(Instruction32 instruct) {
      return "Finished!";
    }
  },

  ADD() {
    @Override
    public void execute(Instruction32 instruct) {
      Register val1 = getRegisterFromLocation(instruct.getValue1());
      Register val2 = getRegisterFromLocation(instruct.getValue2());
      Register out = getRegisterFromLocation(instruct.getValue3());

      Int32 a = val1.getIntValue();
      Int32 b = val2.getIntValue();
      out.setValue(Int32.add(a, b));
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg1 = Register.valueOf(commands[1]);
      if (!reg1.isFloatRegister()) {
        instruction.setValue1(parseInt(reg1.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg1.name());
      }

      Register reg2 = Register.valueOf(commands[2]);
      if (!reg2.isFloatRegister()) {
        instruction.setValue2(parseInt(reg2.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg2.name());
      }

      Register reg3 = Register.valueOf(commands[3]);
      if (!reg3.isFloatRegister()) {
        instruction.setValue3(parseInt(reg3.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg3.name());
      }

      return instruction;
    }
  },

  SUB() {
    @Override
    public void execute(Instruction32 instruct) {
      Register val1 = getRegisterFromLocation(instruct.getValue1());
      Register val2 = getRegisterFromLocation(instruct.getValue2());
      Register out = getRegisterFromLocation(instruct.getValue3());

      Int32 a = val1.getIntValue();
      Int32 b = val2.getIntValue();
      out.setValue(Int32.subtract(a, b));
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg1 = Register.valueOf(commands[1]);
      if (!reg1.isFloatRegister()) {
        instruction.setValue1(parseInt(reg1.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg1.name());
      }

      Register reg2 = Register.valueOf(commands[2]);
      if (!reg2.isFloatRegister()) {
        instruction.setValue2(parseInt(reg2.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg2.name());
      }

      Register reg3 = Register.valueOf(commands[3]);
      if (!reg3.isFloatRegister()) {
        instruction.setValue3(parseInt(reg3.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg3.name());
      }

      return instruction;
    }
  },

  MUL() {
    @Override
    public void execute(Instruction32 instruct) {
      Register val1 = getRegisterFromLocation(instruct.getValue1());
      Register val2 = getRegisterFromLocation(instruct.getValue2());
      Register out = getRegisterFromLocation(instruct.getValue3());

      Int32 a = val1.getIntValue();
      Int32 b = val2.getIntValue();
      out.setValue(Int32.multiply(a, b));
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg1 = Register.valueOf(commands[1]);
      if (!reg1.isFloatRegister()) {
        instruction.setValue1(parseInt(reg1.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg1.name());
      }

      Register reg2 = Register.valueOf(commands[2]);
      if (!reg2.isFloatRegister()) {
        instruction.setValue2(parseInt(reg2.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg2.name());
      }

      Register reg3 = Register.valueOf(commands[3]);
      if (!reg3.isFloatRegister()) {
        instruction.setValue3(parseInt(reg3.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg3.name());
      }

      return instruction;
    }
  },

  DIV() {
    @Override
    public void execute(Instruction32 instruct) {
      Register val1 = getRegisterFromLocation(instruct.getValue1());
      Register val2 = getRegisterFromLocation(instruct.getValue2());
      Register out = getRegisterFromLocation(instruct.getValue3());

      Int32 a = val1.getIntValue();
      Int32 b = val2.getIntValue();
      out.setValue(Int32.divide(a, b));
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg1 = Register.valueOf(commands[1]);
      if (!reg1.isFloatRegister()) {
        instruction.setValue1(parseInt(reg1.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg1.name());
      }

      Register reg2 = Register.valueOf(commands[2]);
      if (!reg2.isFloatRegister()) {
        instruction.setValue2(parseInt(reg2.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg2.name());
      }

      Register reg3 = Register.valueOf(commands[3]);
      if (!reg3.isFloatRegister()) {
        instruction.setValue3(parseInt(reg3.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg3.name());
      }

      return instruction;
    }
  },

  FADD() {
    @Override
    public void execute(Instruction32 instruct) {
      Register val1 = getRegisterFromLocation(instruct.getValue1());
      Register val2 = getRegisterFromLocation(instruct.getValue2());
      Register out = getRegisterFromLocation(instruct.getValue3());

      Float32 a = val1.getFloatValue();
      Float32 b = val2.getFloatValue();
      out.setValue(Float32.add(a, b));
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg1 = Register.valueOf(commands[1]);
      if (reg1.isFloatRegister()) {
        instruction.setValue1(parseInt(reg1.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg1.name());
      }

      Register reg2 = Register.valueOf(commands[2]);
      if (reg2.isFloatRegister()) {
        instruction.setValue2(parseInt(reg2.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg2.name());
      }

      Register reg3 = Register.valueOf(commands[3]);
      if (reg3.isFloatRegister()) {
        instruction.setValue3(parseInt(reg3.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg3.name());
      }

      return instruction;
    }
  },

  FSUB() {
    @Override
    public void execute(Instruction32 instruct) {
      Register val1 = getRegisterFromLocation(instruct.getValue1());
      Register val2 = getRegisterFromLocation(instruct.getValue2());
      Register out = getRegisterFromLocation(instruct.getValue3());

      Float32 a = val1.getFloatValue();
      Float32 b = val2.getFloatValue();
      out.setValue(Float32.subtract(a, b));
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg1 = Register.valueOf(commands[1]);
      if (reg1.isFloatRegister()) {
        instruction.setValue1(parseInt(reg1.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg1.name());
      }

      Register reg2 = Register.valueOf(commands[2]);
      if (reg2.isFloatRegister()) {
        instruction.setValue2(parseInt(reg2.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg2.name());
      }

      Register reg3 = Register.valueOf(commands[3]);
      if (reg3.isFloatRegister()) {
        instruction.setValue3(parseInt(reg3.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg3.name());
      }

      return instruction;
    }
  },

  FMUL() {
    @Override
    public void execute(Instruction32 instruct) {
      Register val1 = getRegisterFromLocation(instruct.getValue1());
      Register val2 = getRegisterFromLocation(instruct.getValue2());
      Register out = getRegisterFromLocation(instruct.getValue3());

      Float32 a = val1.getFloatValue();
      Float32 b = val2.getFloatValue();
      out.setValue(Float32.multiply(a, b));
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg1 = Register.valueOf(commands[1]);
      if (reg1.isFloatRegister()) {
        instruction.setValue1(parseInt(reg1.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg1.name());
      }

      Register reg2 = Register.valueOf(commands[2]);
      if (reg2.isFloatRegister()) {
        instruction.setValue2(parseInt(reg2.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg2.name());
      }

      Register reg3 = Register.valueOf(commands[3]);
      if (reg3.isFloatRegister()) {
        instruction.setValue3(parseInt(reg3.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg3.name());
      }

      return instruction;
    }
  },

  FDIV() {
    @Override
    public void execute(Instruction32 instruct) {
      Register val1 = getRegisterFromLocation(instruct.getValue1());
      Register val2 = getRegisterFromLocation(instruct.getValue2());
      Register out = getRegisterFromLocation(instruct.getValue3());

      Float32 a = val1.getFloatValue();
      Float32 b = val2.getFloatValue();
      out.setValue(Float32.divide(a, b));
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg1 = Register.valueOf(commands[1]);
      if (reg1.isFloatRegister()) {
        instruction.setValue1(parseInt(reg1.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg1.name());
      }

      Register reg2 = Register.valueOf(commands[2]);
      if (reg2.isFloatRegister()) {
        instruction.setValue2(parseInt(reg2.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg2.name());
      }

      Register reg3 = Register.valueOf(commands[3]);
      if (reg3.isFloatRegister()) {
        instruction.setValue3(parseInt(reg3.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot use register " + reg3.name());
      }

      return instruction;
    }
  },

  LOAD() {
    @Override
    public void execute(Instruction32 instruct) {
      Memory address = getMemoryFromLocation(instruct.getValue1());
      Register dest = getRegisterFromLocation(instruct.getValue2());

      if (address.isDataAddress()) {
        dest.setValue(address.getValue());
      }
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Memory address = Memory.values()[Integer.parseInt(commands[1])];
      if (address.isDataAddress()) {
        instruction.setValue1(parseInt(address.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot load from non-data memory location " + address.ordinal());
      }

      Register reg = Register.valueOf(commands[2]);
      instruction.setValue2(parseInt(reg.ordinal()));

      return instruction;
    }

    @Override
    public String instructionToString(Instruction32 instruct) {
      StringBuilder sb = new StringBuilder();

      sb.append(name().toLowerCase());
      sb.append(" ");
      Memory address = getMemoryFromLocation(instruct.getValue1());
      Register dest = getRegisterFromLocation(instruct.getValue2());
      sb.append(address.ordinal());
      sb.append(" ");
      sb.append(dest.name());

      return sb.toString();
    }

  },

  STORE() {
    @Override
    public void execute(Instruction32 instruct) {
      Register source = getRegisterFromLocation(instruct.getValue1());
      Memory address = getMemoryFromLocation(instruct.getValue2());

      if (address.isDataAddress()) {
        address.setValue(source.getValue());
      }
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Register reg = Register.valueOf(commands[1]);
      instruction.setValue1(parseInt(reg.ordinal()));

      Memory address = Memory.values()[Integer.parseInt(commands[2])];
      if (address.isDataAddress()) {
        instruction.setValue2(parseInt(address.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction: " + name().toLowerCase() + " cannot store in non-data location " + address.ordinal());
      }

      return instruction;
    }

    @Override
    public String instructionToString(Instruction32 instruct) {
      StringBuilder sb = new StringBuilder();

      sb.append(name().toLowerCase());
      sb.append(" ");
      Register source = getRegisterFromLocation(instruct.getValue1());
      Memory address = getMemoryFromLocation(instruct.getValue2());
      sb.append(source.name());
      sb.append(" ");
      sb.append(address.ordinal());

      return sb.toString();
    }
  },

  STOREI() {
    @Override
    public void execute(Instruction32 instruct) {
      Int32 value = getIntFromBits(instruct.getValue1());
      Memory address = getMemoryFromLocation(instruct.getValue2());

      if (address.isDataAddress()) {
        address.setValue(value);
      }
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      instruction.setValue1(parseInt(AssemblyFileInterpreter.getNextMapValue()));
      Memory address = Memory.values()[Integer.parseInt(commands[2])];
      if (address.isDataAddress()) {
        instruction.setValue2(parseInt(address.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction: " + name().toLowerCase() + " cannot store in non-data location " + address.ordinal());
      }

      return instruction;
    }

    @Override
    public String instructionToString(Instruction32 instruct) {
      StringBuilder sb = new StringBuilder();

      sb.append(name().toLowerCase());
      sb.append(" ");
      String value = getStringFromBits(instruct.getValue1());
      Memory address = getMemoryFromLocation(instruct.getValue2());
      sb.append(value);
      sb.append(" ");
      sb.append(address.ordinal());

      return sb.toString();
    }
  },

  STOREF() {
    @Override
    public void execute(Instruction32 instruct) {
      Float32 value = getFloatFromBits(instruct.getValue1());
      Memory address = getMemoryFromLocation(instruct.getValue2());

      if (address.isDataAddress()) {
        address.setValue(value);
      }
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      instruction.setValue1(parseInt(AssemblyFileInterpreter.getNextMapValue()));
      Memory address = Memory.values()[Integer.parseInt(commands[2])];
      if (address.isDataAddress()) {
        instruction.setValue2(parseInt(address.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction: " + name().toLowerCase() + " cannot store in non-data location " + address.ordinal());
      }

      return instruction;
    }

    @Override
    public String instructionToString(Instruction32 instruct) {
      StringBuilder sb = new StringBuilder();

      sb.append(name().toLowerCase());
      sb.append(" ");
      String value = getStringFromBits(instruct.getValue1());
      Memory address = getMemoryFromLocation(instruct.getValue2());
      sb.append(value);
      sb.append(" ");
      sb.append(address.ordinal());

      return sb.toString();
    }
  },

  JUMP() {
    @Override
    public void execute(Instruction32 instruct) {
      Memory target = getMemoryFromLocation(instruct.getValue1());

      if (!target.isDataAddress()) {
        ControlUnit.getInstance().setCount(target.ordinal() - 1);
      }
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Memory address = Memory.values()[Integer.parseInt(commands[1])];
      if (!address.isDataAddress()) {
        instruction.setValue1(parseInt(address.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot jump to non-program location " + address.ordinal());
      }

      return instruction;
    }

    @Override
    public String instructionToString(Instruction32 instruct) {
      StringBuilder sb = new StringBuilder();

      sb.append(name().toLowerCase());
      sb.append(" ");
      Memory target = getMemoryFromLocation(instruct.getValue1());
      sb.append(target.ordinal());

      return sb.toString();
    }
  },

  JUMPZ() {
    @Override
    public void execute(Instruction32 instruct) {
      Memory target = getMemoryFromLocation(instruct.getValue1());

      if (!target.isDataAddress() && ControlUnit.getInstance().isZero()) {
        ControlUnit.getInstance().setCount(target.ordinal() - 1);
      }
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Memory address = Memory.values()[Integer.parseInt(commands[1])];
      if (!address.isDataAddress()) {
        instruction.setValue1(parseInt(address.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot jump to non-program location " + address.ordinal());
      }

      return instruction;
    }

    @Override
    public String instructionToString(Instruction32 instruct) {
      StringBuilder sb = new StringBuilder();

      sb.append(name().toLowerCase());
      sb.append(" ");
      Memory target = getMemoryFromLocation(instruct.getValue1());
      sb.append(target.ordinal());

      return sb.toString();
    }
  },

  JUMPO() {
    @Override
    public void execute(Instruction32 instruct) {
      Memory target = getMemoryFromLocation(instruct.getValue1());

      if (!target.isDataAddress() && ControlUnit.getInstance().isOverflow()) {
        ControlUnit.getInstance().setCount(target.ordinal() - 1);
      }
    }

    @Override
    public Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException {
      Instruction32 instruction = new Instruction32();
      instruction.setOpCode(getOpCode());

      Memory address = Memory.values()[Integer.parseInt(commands[1])];
      if (!address.isDataAddress()) {
        instruction.setValue1(parseInt(address.ordinal()));
      } else {
        throw new AssemblyFileInterpreter.AssemblySyntaxException("Instruction:  " + name().toLowerCase() + " cannot jump to non-program location " + address.ordinal());
      }

      return instruction;
    }

    @Override
    public String instructionToString(Instruction32 instruct) {
      StringBuilder sb = new StringBuilder();

      sb.append(name().toLowerCase());
      sb.append(" ");
      Memory target = getMemoryFromLocation(instruct.getValue1());
      sb.append(target.ordinal());

      return sb.toString();
    }
  },
  ;

  private static final int BIT_LENGTH = 8;
  private boolean[] opCode;

  InstructionSet() {
    opCode = parseInt(ordinal());
  }

  public abstract void execute(Instruction32 instruct);

  public boolean[] getOpCode() {
    return opCode;
  }

  public boolean[] parseInt(int num) {
    boolean[] result = new boolean[BIT_LENGTH];

    for (int index = BIT_LENGTH - 1; index >= 0; index--) {
      result[index] = num % 2 != 0;
      num = num / 2;
    }

    return result;
  }

  protected Register getRegisterFromLocation(boolean[] bitArray) {
    for (Register register : Register.values()) {
      if (Arrays.equals(register.getLocation(), bitArray)) {
        return register;
      }
    }
    return null;
  }

  protected Memory getMemoryFromLocation(boolean[] bitArray) {
    for (Memory memory : Memory.values()) {
      if (Arrays.equals(memory.getLocation(), bitArray)) {
        return memory;
      }
    }
    return null;
  }

  protected Int32 getIntFromBits(boolean[] index) {
    Int32 value = new Int32();

    for (boolean[] bits : AssemblyFileInterpreter.rawValues.keySet()) {
      if (Arrays.equals(bits, index)) {
        value = Int32.parseInt32(Integer.parseInt(AssemblyFileInterpreter.rawValues.get(bits)));
      }
    }

    return value;
  }

  protected Float32 getFloatFromBits(boolean[] index) {
    Float32 value = new Float32();

    for (boolean[] bits : AssemblyFileInterpreter.rawValues.keySet()) {
      if (Arrays.equals(bits, index)) {
        value = Float32.parseFloat32(AssemblyFileInterpreter.rawValues.get(bits), Float32.ValueType.DECIMAL);
      }
    }

    return value;
  }

  protected String getStringFromBits(boolean[] index) {
    String value = "";

    for (boolean[] bits : AssemblyFileInterpreter.rawValues.keySet()) {
      if (Arrays.equals(bits, index)) {
        value = AssemblyFileInterpreter.rawValues.get(bits);
      }
    }

    return value;
  }

  public abstract Instruction32 encodeInstruction(String[] commands) throws AssemblyFileInterpreter.AssemblySyntaxException;

  public String instructionToString(Instruction32 instruct) {
    StringBuilder sb = new StringBuilder();

    sb.append(name().toLowerCase());
    sb.append(" ");
    Register val1 = getRegisterFromLocation(instruct.getValue1());
    Register val2 = getRegisterFromLocation(instruct.getValue2());
    Register out = getRegisterFromLocation(instruct.getValue3());
    sb.append(val1.name());
    sb.append(" ");
    sb.append(val2.name());
    sb.append(" ");
    sb.append(out);

    return sb.toString();
  }

}
