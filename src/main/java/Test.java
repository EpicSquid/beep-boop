public class Test {

  /**
   * Tests some values for this project. To add your own, follow the comments at the end of the tests I have included.
   */
  public static void main(String... args) {

    System.out.println("Ints:");

    System.out.println(Int32.parseInt32(2147483647).toString());

    System.out.println(Int32.add(Int32.parseInt32(200000000), Int32.parseInt32(10)).toString());

    System.out.println(Int32.parseInt32(-2147483648));

    System.out.println(Int32.negate(Int32.parseInt32(-1)));

    System.out.println(Int32.subtract(Int32.parseInt32(0), Int32.parseInt32(-1)));

    System.out.println(Int32.multiply(Int32.parseInt32(4), Int32.parseInt32(2)));

    System.out.println(Int32.divide(Int32.parseInt32(8), Int32.parseInt32(-2)));

    System.out.println(Int32.parseInt32("11111111111111111111111111111100"));

    System.out.println("Floats:");

    System.out.println(Float32.parseFloat32("2.5", Float32.ValueType.DECIMAL));

    System.out.println(Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL));

    System.out.println(Float32.parseFloat32("0.00025", Float32.ValueType.DECIMAL));

    System.out.println(Float32.parseFloat32("0.2", Float32.ValueType.DECIMAL));

    System.out.println(Float32.add(Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.125", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.add(Float32.parseFloat32("0.125", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.multiply(Float32.parseFloat32("0.4", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.2", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.multiply(Float32.parseFloat32("4.0", Float32.ValueType.DECIMAL), Float32.parseFloat32("2.0", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.divide(Float32.parseFloat32("0.125", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.0625", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.parseFloat32("-0.25", Float32.ValueType.DECIMAL));

    System.out.println(Float32.subtract(Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.subtract(Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.125", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.subtract(Float32.parseFloat32("0.125", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.add(Float32.parseFloat32("-0.125", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.subtract(Float32.parseFloat32("-0.125", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.25", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.subtract(Float32.parseFloat32("0.1875", Float32.ValueType.DECIMAL), Float32.parseFloat32("0.125", Float32.ValueType.DECIMAL)));

    System.out.println(Float32.parseFloat32("10111110000000000000000000000000", Float32.ValueType.BINARY));

    // Adding 2 Integer Numbers - 4 and 2
    //
    // Create an object representing the decimal number 2 (binary value 00000000000000000000000000000010)
    Int32 two = Int32.parseInt32("00000000000000000000000000000010");
    //

    // Create an object representing the decimal number 4 (binary value 00000000000000000000000000000100)

    Int32 four = Int32.parseInt32("00000000000000000000000000000100");

    //

    // Add two and four

    Int32 sum = Int32.add(two, four);

    //

    // Print the result as a binary value

    System.out.println(sum);

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------
    //
    // INTEGERS:
    //
    // To add your own integer calculations use the following:
    // Int32.parseInt32(int value) - Creates a binary representation of a decimal integer value
    // Int32.parseInt32(String value) - Converts a correctly formatted 32-bit binary string into a binary value
    // Int32.add(Int32 a, Int32 b) - Adds 2 Int32 binary values
    // Int32.subtract(Int32 a, Int32 b) - Subtracts Int32 value b from a
    // Int32.multiply(Int32 a, Int32 b) - Multiplies Int32 value a by b
    // Int32.divide(Int32 a, Int32 b) - Divides Int32 value a by b
    //
    // FLOATS:
    //
    // To add your own single precision floating point calculations use the following:
    // Float32.parseFloat32(String value, Float32.ValueType.DECIMAL) - Creates a binary representation of a decimal floating point value
    // Float32.parseFloat32(String value, Float32.ValueType.BINARY) - Converts a correctly formatted 32-bit binary string into a binary value
    // Float32.add(Float32 a, Float32 b) - Adds 2 Float32 binary values
    // Float32.subtract(Float32 a, Float32 b) - Subtracts Float32 value b from a
    // Float32.multiply(Float32 a, Float32 b) - Multiplies Float32 value a by b
    // Float32.divide(Float32 a, Float32 b) - Divides Float32 value a by b
    //
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------
  }
}