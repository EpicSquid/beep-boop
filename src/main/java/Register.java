public enum Register {

  A,
  B,
  C,
  D,

  E,
  F,
  G,
  H,

  ;

  private static final int BIT_LENGTH = 8;

  private I32BitValue value;
  private boolean[] location;

  Register() {
    value = new Int32();
    location = parseInt(ordinal());
  }

  public I32BitValue getValue() {
    return value;
  }

  public void setValue(I32BitValue value) {
    this.value = value;
  }

  public Int32 getIntValue() {
    if (ordinal() < 4) {
      return new Int32(value.getBitArray());
    }
    return null;
  }

  public Float32 getFloatValue() {
    if (ordinal() >= 4) {
      Float32 result = new Float32();
      result.createFromBitArray(value.getBitArray());
      return result;
    }
    return null;
  }

  public boolean[] getLocation() {
    return location;
  }

  public boolean[] parseInt(int num) {
    boolean[] result = new boolean[BIT_LENGTH];

    for (int index = BIT_LENGTH - 1; index >= 0; index-- ) {
      result[index] = num % 2 != 0;
      num = num / 2;
    }

    return result;
  }

  public boolean isFloatRegister() {
    return ordinal() >= 4;
  }
}
