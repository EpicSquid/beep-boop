import java.util.Arrays;

public class Processor implements ITickListener {

  public static String console = "";

  private boolean finished = false;

  public Processor() {
    ControlUnit.getInstance().addListener(this);
  }

  @Override
  public void onTick() {
    if (finished) {
      return;
    }
    ControlUnit cu = ControlUnit.getInstance();

    boolean[] count = Arrays.copyOfRange(cu.getCount(), Int32.BIT_LENGTH - 8, Int32.BIT_LENGTH);

    Memory address = getMemoryFromLocation(count);
    if (address != null) {
      ControlUnit.getInstance().setInstruction(address.getValue().getBitArray());

      Instruction32 instruct = new Instruction32();
      instruct.createFromBitArray(ControlUnit.getInstance().getInstruction());

      InstructionSet instruction = getInsturctionFromOpCode(instruct.getOpCode());
      if (instruction != null) {
        instruction.execute(instruct);
      }
    } else {
      finished = true;
    }
  }

  protected Memory getMemoryFromLocation(boolean[] bitArray) {
    for (Memory memory : Memory.values()) {
      if (Arrays.equals(memory.getLocation(), bitArray)) {
        return memory;
      }
    }
    return null;
  }

  protected InstructionSet getInsturctionFromOpCode(boolean[] bitArray) {
    for (InstructionSet instruction : InstructionSet.values()) {
      if (Arrays.equals(instruction.getOpCode(), bitArray)) {
        return instruction;
      }
    }
    return null;
  }
}
