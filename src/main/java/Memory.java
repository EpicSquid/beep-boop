public enum Memory {

  // Data
  MEM0,
  MEM1,
  MEM2,
  MEM3,
  MEM4,
  MEM5,
  MEM6,
  MEM7,
  MEM8,
  MEM9,
  MEM10,
  MEM11,
  MEM12,
  MEM13,
  MEM14,
  MEM15,
  MEM16,
  MEM17,
  MEM18,

  // Program
  MEM19,
  MEM20,
  MEM21,
  MEM22,
  MEM23,
  MEM24,
  MEM25,
  MEM26,
  MEM27,
  MEM28,
  MEM29,
  MEM30,
  MEM31,
  MEM32,
  MEM33,
  MEM34,
  MEM35,
  MEM36,
  MEM37,
  MEM38,
  MEM39,

  ;

  private static final int BIT_LENGTH = 8;

  private I32BitValue value;
  private boolean[] location;

  Memory() {
    value = new Int32();
    location = parseInt(ordinal());
  }

  public I32BitValue getValue() {
    return value;
  }

  public void setValue(I32BitValue value) {
    this.value = value;
  }

  public boolean[] getLocation() {
    return location;
  }

  public boolean[] parseInt(int num) {
    boolean[] result = new boolean[BIT_LENGTH];

    for (int index = BIT_LENGTH - 1; index >= 0; index-- ) {
      result[index] = num % 2 != 0;
      num = num / 2;
    }

    return result;
  }

  public boolean isDataAddress() {
    return ordinal() < 20;
  }
}
