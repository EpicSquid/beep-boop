import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Gui extends Application {

  private static List<Text> columnOne = new ArrayList<>();
  private static List<Text> columnTwo = new ArrayList<>();
  private static List<Text> columnThree = new ArrayList<>();
  private static List<Text> columnFour = new ArrayList<>();

  private boolean hasLoaded = false;

  @Override
  public void start(Stage stage) {

    new Processor();

    // Register
    Text gprStatus = new TextExpansion("GPR Status", 1);
    Text valA = new TextExpansion(Register.A.name() + ": " + Register.A.getValue().toString(), 1);
    Text valB = new TextExpansion(Register.B.name() + ": " + Register.B.getValue().toString(), 1);
    Text valC = new TextExpansion(Register.C.name() + ": " + Register.C.getValue().toString(), 1);
    Text valD = new TextExpansion(Register.D.name() + ": " + Register.D.getValue().toString(), 1);
    Text fprStatus = new TextExpansion("FPR Status", 1);
    Text valE = new TextExpansion(Register.E.name() + ": " + Register.E.getValue().toString(), 1);
    Text valF = new TextExpansion(Register.F.name() + ": " + Register.F.getValue().toString(), 1);
    Text valG = new TextExpansion(Register.G.name() + ": " + Register.G.getValue().toString(), 1);
    Text valH = new TextExpansion(Register.H.name() + ": " + Register.H.getValue().toString(), 1);
    Text curStatus = new TextExpansion("CUR Status", 1);
    Text valPC = new TextExpansion("PC: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getCount()), 1);
    Text valIR = new TextExpansion("IR: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getInstruction()), 1);
    Text valPSW = new TextExpansion("PSW: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getStatus()), 1);

    // Memory Labels
    Text memoryStatus = new Text("Memory Status");
    Text blank = new Text("");
    Text program = new TextExpansion("Program (20-39)", 3);
    Text data = new TextExpansion("Data (0-19)", 4);

    // Memory
    for (int i = 0; i < Memory.values().length; i++) {
      Memory mem = Memory.values()[i];
      if (mem.isDataAddress()) {
        new TextExpansion(i + ": " + mem.getValue().toString(), 4);
      } else {
        new TextExpansion(i + ": " + mem.getValue().toString(), 3);
      }
    }

    Text currentInstructionState = new Text("Current Instruction State");
    Text instruction = new Text("Instruction: ");

    Text consoleStatus = new Text("Console");
    Text consoleData = new Text("");

    Button load = new Button("Load Program");
    Button tick = new Button("Clock Tick");

    load.setOnAction((ActionEvent event) -> {
      if (hasLoaded) {
        return;
      }

      AssemblyFileInterpreter afi = new AssemblyFileInterpreter();
      afi.readFile();

      valA.setText(Register.A.name() + ": " + Register.A.getValue().toString());
      valB.setText(Register.B.name() + ": " + Register.B.getValue().toString());
      valC.setText(Register.C.name() + ": " + Register.C.getValue().toString());
      valD.setText(Register.D.name() + ": " + Register.D.getValue().toString());
      valE.setText(Register.E.name() + ": " + Register.E.getValue().toString());
      valF.setText(Register.F.name() + ": " + Register.F.getValue().toString());
      valG.setText(Register.G.name() + ": " + Register.G.getValue().toString());
      valH.setText(Register.H.name() + ": " + Register.H.getValue().toString());
      valPC.setText("PC: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getCount()));
      valIR.setText("IR: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getInstruction()));
      valPSW.setText("PSW: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getStatus()));

      for (int i = 0; i < Memory.values().length; i++) {
        Memory mem = Memory.values()[i];
        if (mem.isDataAddress()) {
          columnFour.get(i + 1).setText(i + ": " + mem.getValue().toString());
        } else {
          columnThree.get(i + 1 - 20).setText(i + ": " + mem.getValue().toString());
        }
      }

      consoleData.setText(Processor.console);
      instruction.setText("Press \"Clock Tick\" to Start!");

      hasLoaded = true;
    });

    tick.setOnAction((ActionEvent event) -> {
      if (!(Processor.console == null || Processor.console.equals(""))) {
        return;
      }

      ControlUnit.getInstance().tick();
      valA.setText(Register.A.name() + ": " + Register.A.getValue().toString());
      valB.setText(Register.B.name() + ": " + Register.B.getValue().toString());
      valC.setText(Register.C.name() + ": " + Register.C.getValue().toString());
      valD.setText(Register.D.name() + ": " + Register.D.getValue().toString());
      valE.setText(Register.E.name() + ": " + Register.E.getValue().toString());
      valF.setText(Register.F.name() + ": " + Register.F.getValue().toString());
      valG.setText(Register.G.name() + ": " + Register.G.getValue().toString());
      valH.setText(Register.H.name() + ": " + Register.H.getValue().toString());
      valPC.setText("PC: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getCount()));
      valIR.setText("IR: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getInstruction()));
      valPSW.setText("PSW: " + ControlUnit.getInstance().toString(ControlUnit.getInstance().getStatus()));

      for (int i = 0; i < Memory.values().length; i++) {
        Memory mem = Memory.values()[i];
        if (mem.isDataAddress()) {
          columnFour.get(i + 1).setText(i + ": " + mem.getValue().toString());
        } else {
          columnThree.get(i + 1 - 20).setText(i + ": " + mem.getValue().toString());
        }
      }

      Instruction32 instruct = new Instruction32();
      instruct.createFromBitArray(ControlUnit.getInstance().getInstruction());
      InstructionSet code = getInsturctionFromOpCode(instruct.getOpCode());
      if (code != null) {
        instruction.setText("Instruction: " + code.instructionToString(instruct));
      }
      consoleData.setText(Processor.console);
    });

    GridPane grid = new GridPane();
    grid.setMinSize(1280, 720);
    grid.setPadding(new Insets(10, 10, 10, 10));
    grid.setHgap(10);
    grid.setVgap(5);
    grid.setAlignment(Pos.CENTER);

    for (int i = 0; i < columnOne.size(); i++) {
      grid.add(columnOne.get(i), 0, i);
    }

    grid.add(memoryStatus, 2, 0);
    grid.add(blank, 3, 0);

    for (int i = 0; i < columnThree.size(); i++) {
      grid.add(columnThree.get(i), 2, i + 1);
      grid.add(columnFour.get(i), 3, i + 1);
    }

    grid.add(load, 4, 0);
    grid.add(tick, 4, 1);

    grid.add(new Text(""), 4, 2);
    grid.add(currentInstructionState, 4, 3);
    grid.add(instruction, 4, 4);

    grid.add(new Text(""), 4, 5);
    grid.add(consoleStatus, 4, 6);
    grid.add(consoleData, 4, 7);

    Scene scene = new Scene(grid);

    stage.setTitle("Processor Emulator Application");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String... args) {
    launch(args);
  }

  protected InstructionSet getInsturctionFromOpCode(boolean[] bitArray) {
    for (InstructionSet instruction : InstructionSet.values()) {
      if (Arrays.equals(instruction.getOpCode(), bitArray)) {
        return instruction;
      }
    }
    return null;
  }

  private static class TextExpansion extends Text {

    public TextExpansion(String text, int column) {
      super(text);
      switch (column) {
        case 1:
          columnOne.add(this);
          break;
        case 2:
          columnTwo.add(this);
          break;
        case 3:
          columnThree.add(this);
          break;
        case 4:
          columnFour.add(this);
          break;
        default:
          columnOne.add(this);
          break;
      }
    }
  }
}
