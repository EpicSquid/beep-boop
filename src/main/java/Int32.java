public class Int32 implements I32BitValue {

  // Defined the bit size of the Integer
  public static final int BIT_LENGTH = 32;

  // Boolean array for storing bits
  private boolean[] bitArray;

  // Instance of Arithmetic Util for doing math
  private static final ArithmeticUtil arithmeticUtil = ArithmeticUtil.getInstance(BIT_LENGTH);

  public Int32() {
    this(new boolean[BIT_LENGTH]);
  }

  public Int32(boolean[] bitArray) {
    this.bitArray = bitArray;
  }

  public void setBit(int index, boolean bit) {
    bitArray[index] = bit;
  }

  public boolean getBit(int index) {
    return bitArray[index];
  }

  public static Int32 negate(Int32 num) {
    return new Int32(arithmeticUtil.negate(num.getBitArray()));
  }

  public static Int32 parseInt32(int num) {
    Int32 result = new Int32();

    boolean negative = num < 0;

    for (int index = BIT_LENGTH - 1; index >= 0; index-- ) {
      result.setBit(index, num % 2 != 0);
      num = num / 2;
    }

    if (negative) {
      result = negate(result);
    }

    return result;
  }

  public static Int32 parseInt32(String binary) {
    char[] chars = binary.toCharArray();
    if (chars.length == BIT_LENGTH) {

      boolean[] num = new boolean[BIT_LENGTH];
      for (int i = 0; i < BIT_LENGTH; i++) {
        num[i] = chars[i] == '1';
      }

      return new Int32(num);
    }
    return null;
  }

  public static Int32 add(Int32 a, Int32 b, boolean isInternal) {
    System.out.println("Performing Integer Addition of " + a.toString() + " and " + b.toString());

    Int32 result = new Int32(arithmeticUtil.add(a.getBitArray(), b.getBitArray()));
    if (!isInternal) {
      ArithmeticUtil.setZero(result.getBitArray());
    }
    return result;
  }

  public static Int32 add(Int32 a, Int32 b) {
    return add(a, b, false);
  }

  public static Int32 subtract(Int32 a, Int32 b) {
    System.out.println("Performing Integer Subtraction of " + a.toString() + " and " + b.toString());

    Int32 result = add(a, negate(b));
    ArithmeticUtil.setZero(result.getBitArray());
    return result;
  }

  public static Int32 multiply(Int32 m, Int32 r) {
    System.out.println("Performing Multiplication Addition of " + m.toString() + " and " + r.toString());

    boolean[] multiply = arithmeticUtil.multiply(m.getBitArray(), r.getBitArray());
    boolean[] result = new boolean[BIT_LENGTH];

    for (int i = 0; i < multiply.length; i++) {
      if (i < result.length) {
        result[result.length - 1 - i] = multiply[multiply.length - 1 - i];
      } else if (multiply[i]) {
        System.out.println("Error: Multiplication Value is too large for a 32 bit integer! The result will not display correctly.");
        break;
      }
    }

    ArithmeticUtil.setZero(result);
    return new Int32(result);
  }

  public static Int32 divide(Int32 n, Int32 d) {
    System.out.println("Performing Division of " + n.toString() + " and " + d.toString());

    Int32 result = new Int32(arithmeticUtil.divideInt(n.getBitArray(), d.getBitArray()));
    ArithmeticUtil.setZero(result.getBitArray());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    for (boolean bit : bitArray) {
      sb.append(bit ? 1 : 0);
    }

    return sb.toString();
  }

  @Override
  public void createFromBitArray(boolean[] bitArray) {
    this.bitArray = bitArray;
  }

  @Override
  public boolean[] getBitArray() {
    return bitArray;
  }
}
