import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AssemblyFileInterpreter {

    public static Map<boolean[], String> rawValues = new HashMap<>();

    private static final int BIT_LENGTH = 8;

    private int nextMemoryLocation = Memory.MEM20.ordinal();
    private static int nextMapValue = 0;

    public void readFile() {

        nextMapValue = 0;
        rawValues.clear();
        nextMemoryLocation = Memory.MEM20.ordinal();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File("src/test.txt")));

            String line;
            while ((line = reader.readLine()) != null) {
                String[] commands = line.split(" ");
                interpretInstruction(commands, line);
            }

            reader.close();

        } catch (IOException e) {
            System.out.println("test.txt not found");
        } catch (AssemblySyntaxException e) {
            Processor.console = "Error: " + e.toString();
        }
    }

    private void interpretInstruction(String[] instruction, String original) throws AssemblySyntaxException {
        if (original.charAt(0) == ';') {
            return;
        }
        if (instruction.length < 2) {
            throw new AssemblySyntaxException("Instruction: \"" + original + "\" does not have enough parameters.");
        }

        Instruction32 encodedInstruction = new Instruction32();
        String operation = instruction[0];

        for (InstructionSet op : InstructionSet.values()) {
            if (operation.equals(op.name().toLowerCase())) {
                try {
                    encodedInstruction = op.encodeInstruction(instruction);
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new AssemblySyntaxException("Instruction: " + op.name() + " does not have enough parameters.\nSee line: \"" + original + "\"");
                }
            }
        }

        if (operation.equals(InstructionSet.STOREI.name().toLowerCase()) || operation.equals(InstructionSet.STOREF.name().toLowerCase())) {
            if (operation.equals(InstructionSet.STOREI.name().toLowerCase()) && instruction[1].contains(".")) {
                throw new AssemblySyntaxException("Instruction: STOREI does not allow floating point numbers.\nSee line: \"" + original + "\"");
            }
            if (operation.equals(InstructionSet.STOREF.name().toLowerCase()) && !instruction[1].contains(".")) {
                throw new AssemblySyntaxException("Instruction: STOREF does not allow integer numbers.\nSee line: \"" + original + "\"");
            }
            rawValues.put(parseInt(nextMapValue), instruction[1]);
            nextMapValue++;
        }

        Memory.values()[nextMemoryLocation].setValue(encodedInstruction);
        nextMemoryLocation++;
    }

    private boolean[] parseInt(int num) {
        boolean[] result = new boolean[BIT_LENGTH];

        for (int index = BIT_LENGTH - 1; index >= 0; index--) {
            result[index] = num % 2 != 0;
            num = num / 2;
        }

        return result;
    }

    public static int getNextMapValue() {
        return nextMapValue;
    }

    public static class AssemblySyntaxException extends Exception {

        private String exception;

        AssemblySyntaxException(String e) {
            this.exception = e;
        }

        @Override
        public String toString() {
            return exception;
        }
    }
}
